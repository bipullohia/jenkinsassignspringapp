package com.verizon.assignment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringJenkinsAssignmentApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringJenkinsAssignmentApplication.class, args);
	}
}
